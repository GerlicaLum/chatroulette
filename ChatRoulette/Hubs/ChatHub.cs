﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatRoulette.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string user, string message)
        {   //Context.ConnectionID
            await Clients.AllExcept(Context.ConnectionId).SendAsync("ReceiveMessage", user, message);
        }
    }
}
