﻿


var connection = new signalR.HubConnectionBuilder().configureLogging(signalR.LogLevel.Trace).withUrl("https://localhost:44358/chatHub").build();

    document.getElementById("sendButton").disabled = true;
connection.on("ReceiveMessage", function (user, message) {
    var msg = message.replace(/&/g, "&amp;".replace(/</g, "&lt;").replace(/>/g, "&gt;"));
    var encodedMsg = user + "<br>" + msg;
    var li = document.createElement("div");
    li.innerHTML = encodedMsg;
    li.setAttribute("class", "droite");
    document.getElementById("messagesList").appendChild(li);
})

    connection.start().then(function () {
        document.getElementById("sendButton").disabled = false;
    }).catch(function (err) {
        return console.error(err.toString());
    });

    document.getElementById("sendButton").addEventListener("click", function (event) {
        var user = document.getElementById("userInput").value;
     
        var message = document.getElementById("messageInput").value;
        var encodedMsg2 = user + "<br>" + message + "<br>"; 
        var receive = document.createElement("div")
        receive.setAttribute("class", "gauche");
        receive.innerHTML = encodedMsg2;
        document.getElementById("messagesList").appendChild(receive);

        connection.invoke("SendMessage", user, message)
            .catch(function (err)
           {
            return console.error(err.toString());
            });
        
     
        event.preventDefault();
    });
    

    
